package hits;

import java.util.Arrays;
import java.util.Scanner;

class Pages implements Comparable<Pages>{
	public String url;
	public int auth;
	
	public Pages(String url, int auth) {
		this.url = url;
		this.auth = auth;
	}

	@Override
	public int compareTo(Pages others) {
		return -1* Integer.valueOf(this.auth).compareTo(others.auth);
	}

	@Override
	public String toString() {
		return url + "\t" + auth;
	}
}
public class HitsAlgo {

	public HitsAlgo() {
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Enter no. of URLs: ");
		int n = sc.nextInt();
		int i,j,k;
		int graph[][] = new int[n][n];
		int oldauth[] = new int[n];
		int auth[] = new int[n];
		int hub[] = new int[n];
		String url[] = new String[n];
		Pages page[] = new Pages[n];
		
		System.out.println("Enter urls:");
		for(i=0;i<n;i++) {
			url[i] = sc.next();
		}
		
		System.out.println("Enter Matrix:");
		for(i=0;i<n;i++) {
			for(j=0;j<n;j++) {
				graph[i][j] = sc.nextInt();
			}
		}
		
		for(i=0;i<n;i++) {
			for(j=0;j<n;j++) {
				System.out.println(graph[i][j]);
			}
		}
		for(i=0;i<n;i++)
			hub[i] = 1;
		
		for(k=0;k<n;k++) {
			for(i=0;i<n;i++) {
				auth[i] = 0;
				for(j=0;j<n;j++)
					auth[i] += graph[j][i]*hub[j];
			}
			
			for(i=0;i<n;i++) {
				hub[i] = 0;
				for(j=0;j<n;j++)
					hub[i] += graph[i][j]*auth[j];
			}
		}
		
		System.out.print("Auth:");
		for(i=0;i<n;i++) {
				System.out.print("\t"+auth[i]);
		}
		
		System.out.print("\nHub:");
		for(i=0;i<n;i++) {
				System.out.print("\t"+hub[i]);
		}
		
		for(i=0;i<n;i++) {
			page[i] = new Pages(url[i],auth[i]);
		}
		
		Arrays.sort(page);
		
		System.out.println("\nPages in order:");
		System.out.println("URL\tAuth");
		
		for(i=0;i<n;i++) {
			System.out.println(page[i]);
		}
		sc.close();
	}

	public static void main(String[] args) {
		new HitsAlgo();
	}
}
