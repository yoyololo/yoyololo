declare variable $nl := "&#10;";

let $str := "All employees having salary greater than 20000"
return ($nl,$str,$nl),

for $x in doc("Employee.xml")/EMS/Employee
return if ($x/Salary > 20000) then ($x/Name,$x/Salary) else (),

let $str := "Department having all employees salary less than 10000"
return ($nl,$nl,$str),

for $x in doc("Employee.xml")/EMS/Employee
let $d := $x/Dept
group by $d
return if(max($x/Salary)<10000) then ($nl,$d) else (),

let $str := "Employees having 10 or more years of experience"
return ($nl,$nl,$str,$nl),

for $x in doc("Employee.xml")/EMS/Employee
return if($x/Experience>10) then ($x/Name) else ()