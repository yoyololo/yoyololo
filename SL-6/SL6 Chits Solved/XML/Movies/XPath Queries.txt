Run in XPath/XQuery Builder

Which movies have a summary?
doc("Movies.xml")/movies/movie[summary]/title

Which movies do not have a summary?
doc("Movies.xml")/movies/movie[not(summary)]/title

Titles of the movies published more than 5 years ago
doc("Movies.xml")/movies/movie[(year-from-date(current-date()) - year)>5]/title

What is the last movie of the document?
doc("Movies.xml")/movies/movie[last()]/title

Get the movies whose title contains a “V”
doc("Movies.xml")/movies/movie[contains(title,"V")]/title

Get the movies whose cast consists of exactly three actors
doc("Movies.xml")/movies/movie[count(actor) =3]/title

List the movies published after 2002, including their title and year
doc("Movies.xml")/movies/movie[year>2002]/(title,genre)

Give the title of movies where the director is also one of the actors.
doc("Movies.xml")/movies/movie[director/first_name = actor/first_name and director/last_name = actor/last_name and director/birth_date= actor/birth_date]/title

Show the movies, grouped by genre.
for $g in distinct-values(//genre)
return ($g,//movie[genre=$g]/title),