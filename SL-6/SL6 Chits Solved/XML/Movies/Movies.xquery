declare variable $nl := "&#10;"; (: New Line :)

let $str := concat($nl,"Which movies have a summary?",$nl)
return $str,

for $x in doc("Movies.xml")/movies/movie
return 
if(exists($x/summary)) then $x/title else (),

let $str := concat($nl,$nl,"Which movies do not have a summary?",$nl)
return $str,

for $x in doc("Movies.xml")/movies/movie
return 
if(not(exists($x/summary))) then $x/title else (),

let $str := concat($nl,$nl,"Titles of the movies published more than 5 years ago",$nl)
return $str,

let $curr_year := year-from-date(current-date())
for $x in doc("Movies.xml")/movies/movie
return if(($curr_year - $x/year)>5) then $x/title else (),

let $str := concat($nl,$nl,"What is the last movie of the document?",$nl)
return $str,

let  $x := doc("Movies.xml")/movies/movie[last()]
return $x/title,

let $str := concat($nl,$nl,"Get the movies whose title contains a 'V'",$nl)
return $str,

for $x in doc("Movies.xml")/movies/movie
return if(contains($x/title,"V")) then $x/title else (),

let $str := concat($nl,$nl,"Get the movies whose cast consists of exactly three actors",$nl)
return $str,

for $x in doc("Movies.xml")/movies/movie
return if(count($x/actor) = 3) then $x/title else (),

let $str := concat($nl,$nl,"Get the movies whose cast consists of exactly three actors",$nl)
return $str,

for $x in doc("Movies.xml")/movies/movie
return if(count($x/actor) = 3) then $x/title else (),


let $str := concat($nl,$nl,"List the movies published after 2002, including their title and year",$nl)
return $str,

for $x in doc("Movies.xml")/movies/movie
return if($x/year>2002) then ($x/title,$x/year) else (),

let $str := concat($nl,$nl,"Give the title of movies where the director is also one of the actors.",$nl)
return $str,

for $x in doc("Movies.xml")/movies/movie
where $x/actor/first_name = $x/director/first_name and $x/actor/last_name = $x/director/last_name and $x/actor/birth_date = $x/director/birth_date
return $x/title,

let $str := concat($nl,$nl,"Show the movies, grouped by genre.",$nl)
return $str,

for $x in doc("Movies.xml")/movies/movie
let $g := $x/genre
group by $g
return ($g,$nl,$x/title,$nl)