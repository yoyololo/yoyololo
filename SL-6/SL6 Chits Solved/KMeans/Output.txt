Enter Number of Cluster: 3
Enter Number of Elements: 6
Enter x & y:
0: 1 2
1: 3 4
2: 2 6
3: 7 8
4: 9 5
5: 10 11
------ Pass 1 -------
Cluster 0
0: (1.0,2.0)
	Centroid: (1.0,2.0)
Cluster 1
1: (3.0,4.0)
4: (9.0,5.0)
	Centroid: (6.0,4.5)
Cluster 2
2: (2.0,6.0)
3: (7.0,8.0)
5: (10.0,11.0)
	Centroid: (6.333333333333333,8.333333333333334)
------ Pass 2 -------
Cluster 0
0: (1.0,2.0)
1: (3.0,4.0)
2: (2.0,6.0)
	Centroid: (2.0,4.0)
Cluster 1
4: (9.0,5.0)
	Centroid: (9.0,5.0)
Cluster 2
3: (7.0,8.0)
5: (10.0,11.0)
	Centroid: (8.5,9.5)
------ Pass 3 -------
Cluster 0
0: (1.0,2.0)
1: (3.0,4.0)
2: (2.0,6.0)
	Centroid: (2.0,4.0)
Cluster 1
4: (9.0,5.0)
	Centroid: (9.0,5.0)
Cluster 2
3: (7.0,8.0)
5: (10.0,11.0)
	Centroid: (8.5,9.5)