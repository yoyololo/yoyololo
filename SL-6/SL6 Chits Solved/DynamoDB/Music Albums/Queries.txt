/* //Create Table
var params = {
    TableName: 'Albums',
    KeySchema: [ 
        { 
            AttributeName: 'Album_Name',
            KeyType: 'HASH',
        },
        {
            AttributeName: 'Producer', 
            KeyType: 'RANGE', 
        }
    ],
    AttributeDefinitions: [ 
        {
            AttributeName: 'Album_Name',
            AttributeType: 'S', 
        },
        {
            AttributeName: 'Producer',
            AttributeType: 'S', 
        }
    ],
    ProvisionedThroughput: { // required provisioned throughput for the table
        ReadCapacityUnits: 1, 
        WriteCapacityUnits: 1, 
    }
};
dynamodb.createTable(params, function(err, data) {
    if (err) ppJson(err); // an error occurred
    else ppJson(data); // successful response

});
*/

/* //Describe Table
var params = {
    TableName: 'Albums',
};
dynamodb.describeTable(params, function(err, data) {
    if (err) ppJson(err); // an error occurred
    else ppJson(data); // successful response
});
*/

/* //List Tables
var params = {
};
dynamodb.listTables(params, function(err, data) {
    if (err) ppJson(err); // an error occurred
    else ppJson(data); // successful response

});
*/

/* //Single Insert
var params = {
    TableName: 'Albums',
    Item: { 
        Album_Name: 'Lemonade',
        No_of_tracks: 3,
        Price: 800,
        Producer:'ABCD',
        Artist: 'XQWER'
    }
};
docClient.put(params, function(err, data) {
    if (err) ppJson(err); // an error occurred
    else ppJson(data); // successful response
});
*/

/* //Muliple Insert
var params = {
    RequestItems: { 
        Albums: [ 
            { 
                PutRequest: {
                    Item: {   
                        Album_Name: 'Lemonade',
                        No_of_tracks: 5,
                        Artist: 'Beyonce', 
                        Producer: 'Timber', 
                        Price:600
                    }
                }
            },
            { 
                PutRequest: {
                    Item: {   
                        Album_Name: 'Sound Mirrors',
                        No_of_tracks: 4,
                        Artist: 'Coldcut', 
                        Producer: 'WhoKnows', 
                        Price:300
                    }
                }
            }
        ]
    }
};
docClient.batchWrite(params, function(err, data) {
    if (err) ppJson(err); // an error occurred
    else ppJson(data); // successful response
});
*/
//Display Entire Table
var params = {
    TableName: 'Albums'
};
docClient.scan(params, function(err, data) {
    if (err) ppJson(err); // an error occurred
    else ppJson(data); // successful response
});

//a. All albums having price greater than 500
var params = {
    TableName: 'Albums',
    FilterExpression: '#pr > :v', 
    ExpressionAttributeNames: { 
        '#pr': 'Price'
    },
    ExpressionAttributeValues: { 
        ':v': 500
    }
};
docClient.scan(params, function(err, data) {
    if (err) ppJson(err); // an error occurred
    else ppJson(data); // successful response
});

// b. List all tracks of any single artist
var params = {
    TableName: 'Albums',
    FilterExpression: 'Artist = :value',
    ExpressionAttributeValues: { 
      ':value': 'Beyonce'
    },
    ProjectionExpression: "Artist,No_of_tracks"
};
docClient.scan(params, function(err, data) {
    if (err) ppJson(err); // an error occurred
    else ppJson(data); // successful response
});

/* c. All albums order by producers - All albums not possible, unless dummy HASH key taken as a dummy with the value same for all Items */

// Ascending Order
var params = {
    TableName: 'Albums',
    KeyConditionExpression: "Album_Name=:v",
    ExpressionAttributeValues: { 
      ':v': 'Lemonade'
    },
    ScanIndexForward: true,
    ProjectionExpression: "Album_Name,Producer"
};
docClient.query(params, function(err, data) {
    if (err) ppJson(err); // an error occurred
    else ppJson(data); // successful response
});

//Descending Order
var params = {
    TableName: 'Albums',
    KeyConditionExpression: "Album_Name=:v",
    ExpressionAttributeValues: { 
      ':v': 'Lemonade'
    },
    ScanIndexForward: false,
    ProjectionExpression: "Album_Name,Producer"
};
docClient.query(params, function(err, data) {
    if (err) ppJson(err); // an error occurred
    else ppJson(data); // successful response
});
