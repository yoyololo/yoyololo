package elec;

import java.text.*;
import java.util.*;
import javax.persistence.*;

public class MainClass {
	Scanner sc = new Scanner(System.in);
	EntityManagerFactory emf = Persistence.createEntityManagerFactory("gadgets.odb");;
	EntityManager em = emf.createEntityManager();
	
	public void insert() {
		System.out.print("Number of Entries: ");
		int i,price,n = sc.nextInt();
		String name,datestring;
		Date date = new Date();
		
		em.getTransaction().begin();
		for(i=0;i<n;i++) {
			System.out.print("Enter Name, Price and Date(dd/mm/yyyy): ");
			name = sc.next();
			price = sc.nextInt();
			datestring = sc.next();
			try {
				date = new SimpleDateFormat("dd/MM/yyyy").parse(datestring);
			}
			catch(ParseException e) {
				e.printStackTrace();
			}
			Gadgets g1 = new Gadgets(name, price, date);
			
			em.persist(g1);
		}
		em.getTransaction().commit();
	}
	
	public void priceGreater() {
		em.getTransaction().begin();
		TypedQuery<Gadgets> q = em.createQuery("SELECT FROM Gadgets WHERE price > 10000",Gadgets.class);
		List<Gadgets> gadlist = q.getResultList();
		if(gadlist.size()>0) {
			System.out.println("Price greater than 10000");
			System.out.println("Name\tPrice\tDate of Manufacturing");
			for(Gadgets temp:gadlist) {
				System.out.println(temp);
			}
		}
		else {
			System.out.println("No Records");
		}
		em.getTransaction().commit();
	}
	
	public void order() {
		em.getTransaction().begin();
		TypedQuery<Gadgets> q = em.createQuery("SELECT FROM Gadgets ORDER BY manufacturing",Gadgets.class);
		List<Gadgets> gadlist = q.getResultList();
		if(gadlist.size()>0) {
			System.out.println("\nAscending order of date of manufacturing");
			System.out.println("Name\tPrice\tDate of Manufacturing");
			for(Gadgets temp:gadlist) {
				System.out.println(temp);
			}
		}
		else {
			System.out.println("No Records");
		}
		em.getTransaction().commit();
 	}
	
	public void priceRange() {
		em.getTransaction().begin();
		TypedQuery<Gadgets> q = em.createQuery("SELECT FROM Gadgets WHERE price BETWEEN 10000 AND 15000",Gadgets.class);
		List<Gadgets> gadlist = q.getResultList();
		if(gadlist.size()>0) {
			System.out.println("\nPrice in range 10000 to 15000");
			System.out.println("Name\tPrice\tDate of Manufacturing");
			for(Gadgets temp:gadlist) {
				System.out.println(temp);
			}
		}
		else {
			System.out.println("No Records");
		}
		em.getTransaction().commit();
	}
	
	public MainClass() {
		insert();
		priceGreater();
		order();
		priceRange();
		em.close();
		emf.close();
		sc.close();
	}
	
	public static void main(String[] args) {
		new MainClass();
	}
}
