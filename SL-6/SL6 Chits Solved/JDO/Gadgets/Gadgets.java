package elec;

import java.text.SimpleDateFormat;
import java.util.Date;
import javax.persistence.*;

@Entity
public class Gadgets {
	@Id String name;
	int price;
	Date manufacturing;
	public Gadgets(String name, int price, Date manufacturing) {
		this.name = name;
		this.price = price;
		this.manufacturing = manufacturing;
	}
	
	@Override
	public String toString() {
		return name + "\t" + price + "\t" + new SimpleDateFormat("dd/MM/yyyy").format(manufacturing);
	}
}
