package emp;
import javax.persistence.*;
import java.util.*;
public class MainClass {
	Scanner sc = new Scanner(System.in);
	EntityManagerFactory emf = Persistence.createEntityManagerFactory("employee.odb");
	EntityManager em = emf.createEntityManager();
	
	public void insert() {
		int i,n;
		char grade;
		String name;
		double salary;
		
		em.getTransaction().begin();
		
		System.out.print("Enter Number of Entries: ");
		n = sc.nextInt();
		
		for(i=0;i<n;i++) {
			System.out.print("Enter Name, Salary & Grade: ");
			name = sc.next();
			salary = sc.nextDouble();
			grade = sc.next(".").charAt(0);
			Employee emp = new Employee(name, salary, grade);
			em.persist(emp);
		}
		
		em.getTransaction().commit();
	}
	
	//Display All Employees
	public void query1() {
		em.getTransaction().begin();
		TypedQuery<Employee> q = em.createQuery("SELECT FROM Employee", Employee.class);
		List<Employee> temp = q.getResultList();
		if(temp.size()>0) {
			for(Employee e1:temp) {
				System.out.println(e1);
			}
		}
		else {
			System.out.println("No Records Found");
		}
		em.getTransaction().commit();
	}
	
	//Employees having salary>20000
	public void query2() {
		em.getTransaction().begin();
		TypedQuery<Employee> q = em.createQuery("SELECT FROM Employee WHERE salary>20000", Employee.class);
		List<Employee> temp = q.getResultList();
		if(temp.size()>0) {
			for(Employee e1:temp) {
				System.out.println(e1);
			}
		}
		else {
			System.out.println("No Records Found");
		}
		em.getTransaction().commit();
	}
	
	//Employees in ascending order of increment
	public void query3() {
		em.getTransaction().begin();
		TypedQuery<Employee> q = em.createQuery("SELECT FROM Employee ORDER BY salary ASC", Employee.class);
		List<Employee> temp = q.getResultList();
		if(temp.size()>0) {
			for(Employee e1:temp) {
				System.out.println(e1);
			}
		}
		else {
			System.out.println("No records found");
		}
		em.getTransaction().commit();
	}
	
	//Employees having performance grade in between A to C in order
	public void query4() {
		em.getTransaction().begin();
		TypedQuery<Employee> q = em.createQuery("SELECT FROM Employee WHERE grade BETWEEN 'A' AND 'C' ORDER BY grade", Employee.class);
		List<Employee> temp = q.getResultList();
		if(temp.size()>0) {
			for(Employee e1:temp) {
				System.out.println(e1);
			}
		}
		else {
			System.out.println("No Records Found");
		}
		em.getTransaction().commit();
	}
	public MainClass() {
		int ch;
		
		do {
			System.out.println("\n\tMenu:");
			System.out.println("1. Insert");
			System.out.println("2. Display all Employees");
			System.out.println("3. Employees having salary>20000");
			System.out.println("4. Employees in ascending order of increment");
			System.out.println("5. Employees having performance grade in between A to C in order");
			System.out.println("6. Exit");
			System.out.print("Enter Choice: ");
			ch = sc.nextInt();
			
			switch(ch) {
				case 1: insert();
						break;
				case 2: query1();
						break;
				case 3: query2();
						break;
				case 4: query3();
						break;
				case 5: query4();
						break;	
				case 6: System.out.println("\tExit");
						break;
				default: System.out.println("Invalid Option");
			}
		}while(ch!=6);
		
		sc.close();
		em.close();
		emf.close();
	}
	
	public static void main(String[] args) {
		new MainClass();
	}
}
