package emp;
import javax.persistence.*;

@Entity
public class Employee {
	@Id String name;
	Double salary;
	char grade;

	public Employee() {
	}
	
	public Employee(String name,Double salary,char grade) {
		this.name = name;
		this.salary = salary;
		this.grade = grade;
	}
	
	@Override
	public String toString() {
		return "Employee [name=" + name + ", salary=" + salary + ", grade=" + grade + "]";
	}

	public void setEmployee(String name,Double salary,char grade) {
		this.name = name;
		this.salary = salary;
		this.grade = grade;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setSalary(Double salary) {
		this.salary = salary;
	}
	
	public void setGrade(char grade) {
		this.grade = grade;
	}
}
