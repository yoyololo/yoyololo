import com.mongodb.client.MongoDatabase; 
import com.mongodb.MongoClient; 
import org.bson.Document; 
import com.mongodb.client.MongoCollection;
import java.io.*;
import java .net.*;
import java.util.Scanner;
public class Client
 {
    public static void main(String args[])
    {
        try 
        {
            Scanner scanner = new Scanner(System.in);
            String data;
            int client_response = 1;
            int client_id;

            MongoClient mongo = new MongoClient();
          	System.out.println("Connected to the database successfully"); 

			MongoDatabase db = mongo.getDatabase("test");
			MongoCollection<Document> collection = db.getCollection("TwoPhase");

			System.out.println("Enter Data to Insert into Collection TwoPhase: ");
			data = scanner.next();

            Socket socket = new Socket("localhost",6000);
			System.out.println("Connected to Server.");

            DataInputStream dataInputStream = new DataInputStream(socket.getInputStream());
            client_id = (int)dataInputStream.read();

            String server_response = dataInputStream.readUTF(); //read a string encoded in UTF-8
            if(server_response.equals("Query"))
            {
                System.out.println("Please Choose An Operation:\n1:Commit\n2:Abort");
                client_response = scanner.nextInt();
            }
            DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());
            dataOutputStream.write(client_response);

            server_response = dataInputStream.readUTF();

            System.out.println("Received "+server_response+" From Server");

            if(server_response.equals("Global Commit"))
            {
           		System.out.println("\nInserting "+data+" into Collection TwoPhase");
				Document document = new Document("Client_ID", client_id)
				.append("Data", data);
		        collection.insertOne(document); 
			}
			else
			{
				System.out.println("Insertion into database Aborted");
			}

            dataInputStream.close();
            dataOutputStream.close();
            socket.close();
            mongo.close();

        } 
        catch (IOException e) 
        {
        	System.out.println("**** ERROR ******");
            e.printStackTrace();
        }
    }
}
