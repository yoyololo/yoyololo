import java.io.*;
import java.net.*;
import java.util.Scanner;

public class Server
{
    public static void main(String[] args)
    {
        try
        {
        	int i, num_clients;
        	String server_response;
        	Scanner scanner = new Scanner(System.in);
            ServerSocket server_socket=new ServerSocket(6000);  
			System.out.println("Server Started.");

			System.out.println("\nEnter Number of Clients:");
			num_clients = scanner.nextInt();

			Socket client_socket[] = new Socket[num_clients];
			DataOutputStream dataOutputStream[] = new DataOutputStream[num_clients]; //Allocate Memory later
			DataInputStream dataInputStream[] = new DataInputStream[num_clients];
			int client_response[] = new int[num_clients];

			System.out.println("\nWaiting For "+num_clients+" Clients to Connect");

			for(i=0;i<num_clients;i++)
			{
        	    client_socket[i] = server_socket.accept();
        	    dataOutputStream[i] = new DataOutputStream(client_socket[i].getOutputStream());
        	    dataOutputStream[i].write(i); //Write Client ID to Client
                System.out.println("Client "+i+" Connected");
            }

       	    for(i=0;i<num_clients;i++)
			{
        	    dataOutputStream[i].writeUTF("Query");
        	    dataOutputStream[i].flush();
        	}

            System.out.println("Query Sent To All Clients");

            for(i=0;i<num_clients;i++)
			{
       		    dataInputStream[i] = new DataInputStream(client_socket[i].getInputStream());
       	 	    client_response[i] = (int)dataInputStream[i].read();
              	System.out.println("Response "+client_response[i]+" From Client "+i+" Received");
          	}

          	for(i=0;i<num_clients && client_response[i] == 1;i++)
			{
				//client_response[i] == 1 ensures all are Commit
			}

            if(i==num_clients)
            	server_response = "Global Commit";
            else
        		server_response = "Global Abort";
            
            System.out.println("Sending Response "+server_response+" to All Clients");
            for(i=0;i<num_clients;i++)
			{
        	    dataOutputStream[i].writeUTF(server_response);
  		    	dataOutputStream[i].flush();
            }
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
    }
}