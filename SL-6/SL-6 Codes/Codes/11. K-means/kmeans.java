/* SL-6 11. Implement any one machine learning algorithm for classification / clustering task in Big
data Analytics */

import java.util.Scanner;
import java.util.Random;
import java.util.ArrayList;
import java.util.HashMap;

public class kmeans {

    final int max_v = 100;
    int clusters_total;
    int epoch = 0;
    final int MAX_EPOCHS = 1000;
    ArrayList<cluster> clusters = new ArrayList<>();
    ArrayList<data> datas = new ArrayList<>();
    HashMap<Integer, Integer> hm = new HashMap<>();
    HashMap<Integer, ArrayList<data>> ans = new HashMap<>();

    class data {

        final int data_hash;
        cluster c;
        final float x, y;

        data(float x, float y, int data_hash) {
            this.x = x;
            this.y = y;
            this.data_hash = data_hash;
        }

    }

    class cluster {

        final int cluster_no;
        float x, y;
        float sum_x = 0, sum_y;
        int num = 0;

        cluster(float x, float y, int cluster_no) {
            this.x = x;
            this.y = y;
            this.cluster_no = cluster_no;
        }

        float distance(float x, float y) {
            return (float) (Math.pow(this.x - x, 2) + Math.pow(this.y - y, 2));
        }

        void sum_add(float sumx, float sumy) {
            this.sum_x += sumx;
            this.sum_y += sumy;
            num++;
        }

        void new_centroid() {
            this.x = this.sum_x / num;
            this.y = this.sum_y / num;
            sum_x = 0;
            sum_y = 0;
            num = 0;
        }

    }

    void find_cluster(data d) {
        float min = Float.MAX_VALUE;
        cluster c_final = null;
        for (cluster c : clusters) {
            float dis = c.distance(d.x, d.y);
            if (dis < min) {
                min = dis;
                c_final = c;
            }//end if
        }//end for
        d.c = c_final;

    }

    void assign() {
        for (data d : datas) {
            find_cluster(d);
        }
    }//end assign

    void calc_new_centroid() {
        for (data d : datas) {
            d.c.sum_add(d.x, d.y);
        }

        for (cluster c : clusters) {
            c.new_centroid();
        }
    }//end func

    boolean stop_check() {
        boolean b = true;
        for (data d : datas) {
            int c_no = hm.get(d.data_hash);
            if (c_no != d.c.cluster_no) {
                b = false;
                break;
            }//end if
        }//end for

        return b;
    }

    void dump_data() {
        hm.clear();
        for (data d : datas) {
            hm.put(d.data_hash, d.c.cluster_no);
        }
    }

    void populate_ans() {
        for (data d : datas) {
            if (ans.get(d.c.cluster_no) == null) {
                ans.put(d.c.cluster_no, new ArrayList<data>());
            }

            ans.get(d.c.cluster_no).add(d);
        }

        for (cluster c : clusters) {
            System.out.println("Cluster No. " + c.cluster_no + " || Centroid -> " + c.x + " " + c.y);
            try{
                ArrayList<data> ar = ans.get(c.cluster_no);
                for (data i : ar) {
                    System.out.println("(#" + i.data_hash + ",x:" + i.x + ",y:" + i.y + ")");
                }
                System.out.println();
            }catch(Exception e){
                System.out.println("No Element in cluster");
            }          
        }
    }
    
    void get_input(){
    	Scanner s = new Scanner(System.in);
        int inputs = 0;
        clusters_total = 1;
       	do{
        	System.out.println("Enter number of clusters:");
		    clusters_total = s.nextInt();   
		    System.out.println("Enter number of input data:");
		    inputs = s.nextInt();
        }while(inputs<clusters_total);

        for (int i = 0; i < inputs; i++) {
        	System.out.println("Enter x and y:");
            datas.add(new data(s.nextInt(), s.nextInt(), i));
        }

        for (int i = 0; i < clusters_total; i++) {
            clusters.add(new cluster(datas.get(i).x,datas.get(i).y, i));
        }

    }

    void run_epochs(){
        boolean first = true;
        for (int i = 0; i < MAX_EPOCHS; i++) {
            epoch++;
            assign();
            calc_new_centroid();
            if (!first) {
                if (stop_check()) {
                    break;
                }
            }//end if
            first = false;
            dump_data();
        }//end for
        System.out.println("Number of passes are: " + epoch);
    }
    
    kmeans() {      
        get_input();
        run_epochs();
        populate_ans();
    }//end fun

    public static void main(String args[]) {
        new kmeans();
    }
}

/* Output:
Enter number of clusters:
3
Enter number of input data:
6
Enter x and y:
1 2
Enter x and y:
3 4
Enter x and y:
2 6 
Enter x and y:
7 8
Enter x and y:
9 5
Enter x and y:
10 11
Number of passes are: 3
Cluster No. 0 || Centroid -> 2.0 4.0
(#0,x:1.0,y:2.0)
(#1,x:3.0,y:4.0)
(#2,x:2.0,y:6.0)

Cluster No. 1 || Centroid -> 9.0 5.0
(#4,x:9.0,y:5.0)

Cluster No. 2 || Centroid -> 8.5 9.5
(#3,x:7.0,y:8.0)
(#5,x:10.0,y:11.0)
*/