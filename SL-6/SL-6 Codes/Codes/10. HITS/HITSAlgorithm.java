import java.util.Scanner;  
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

class Pages implements Comparable<Pages> {
    public String url;
    public int auth;

    public Pages(String url, int auth){
    	this.url = url;
    	this.auth = auth;
    }

    @Override
    public int compareTo(Pages other) {
        return -1 * Integer.valueOf(this.auth).compareTo(other.auth);
    }
}

public class HITSAlgorithm {
	public static void main(String args[]) {
		HITSAlgo h = new HITSAlgo();
		Scanner sc=new Scanner(System.in);
		
		System.out.println("Enter No. of URLs: ");
		int size = sc.nextInt();
		int i, j;
		int [][] graph = new int[size] [size] ; 
		int [][] transpose = new int [size][size];
		int [] hub = new int[size];
		int [] auth = new int[size];
		String [] url = new String[size];
		Pages[] pageArray = new Pages[size];
		
		System.out.println("\nEnter URLs:");
		for ( i = 0; i < size; i ++ ){
		    url[i] = sc.next();
		}
		
		System.out.println("\nEnter Adjacency Matrix:");
		for ( i = 0; i < size; i ++ ){
			for(j = 0; j < size; j++){
		         graph[i][j] = sc.nextInt();
		    }
		}
		   	   
		h.transMatrix(graph,transpose,size);
		
		//Initialize Hub Weights
		for ( i = 0; i < size; i ++ ){
			hub[i]=1;
		}
		
		for(j=0;j<size;j++){
			h.updateWeight(transpose, hub, auth, size);		//transpose_of_matrix * hub
			h.updateWeight(graph, auth, hub, size);		//grpah * hub
		}
		
		for ( i = 0; i < size; i ++ ){
		         pageArray[i] = new Pages(url[i],auth[i]);
		}

		Arrays.sort(pageArray);

		System.out.println("\nPages sorted according to rank:");
		System.out.println("URL\tAuthority Weight");
		for ( i = 0; i < size; i ++ ){
			System.out.println(pageArray[i].url+"\t"+pageArray[i].auth);
		}
		
		System.out.println("");
		sc.close();  
	}
	
	public void transMatrix(int matrix[][],int transpose[][],int n){
		int c,d;
		for ( c = 0 ; c < n ; c++ ){
			for ( d = 0 ; d < n ; d++ )               
				transpose[d][c] = matrix[c][d];
	    }
	}
	
	public void updateWeight(int a[][],int b[],int c[],int n){
		int i,j;

		for ( i = 0; i < n; i++){
			c[i]=0;
			for (j = 0; j < n; j++){
					c[i] = c[i] + a[i][j] * b[j];
			}
		}
	}
}

/*
G:\Rajat\Engineering\BE\Sem 2\SL 6\Codes\HITS>javac HITSAlgorithm.java

G:\Rajat\Engineering\BE\Sem 2\SL 6\Codes\HITS>java HITSAlgorithm
Enter No. of URLs:
5

Enter URLs:
mitpune
mitwpu
youtube
mitsog
bhartiyachhatrasansad

Enter Adjacency Matrix:
1 1 1 1 1
1 1 0 1 0
0 0 1 0 0
0 0 0 1 0
0 0 0 0 0

Pages sorted according to rank:
URL     Authority Weight
mitsog  8574
mitpune 7440
mitwpu  7440
youtube 5000
bhartiyachhatrasansad   4338

*/
