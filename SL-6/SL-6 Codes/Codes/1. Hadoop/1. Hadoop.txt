SL 6 1. Study and Configure Hadoop

sudo apt-get install openssh-server

ssh-keygen -t rsa -P ""

cat ~/.ssh/id_rsa.pub >> /home/hadoop/.ssh/authorized_keys

chmod 0666 ~/.ssh/authorized_keys

sudo restart ssh

### Download hadoop-2.7.2.tar.gz

cd /home/student/Downloads

tar xzf hadoop-2.7.2.tar.gz

sudo mv hadoop-2.7.2 hadoop

sudo mv hadoop /home/student/hadoop

cd /home/student/hadoop

su student

gedit ~/.bashrc

### Add follwoing to ~/.bashrc

export JAVA_HOME=/usr/lib/jvm/java-1.7.0-openjdk-i386

export PATH=$PATH:$JAVA_HOME/bin

export HADOOP_HOME=/home/student/hadoop

export HADOOP_MAPRED_HOME=$HADOOP_HOME

export HADOOP_COMMON_HOME=$HADOOP_HOME

export HADOOP_HDFS_HOME=$HADOOP_HOME

export YARN_HOME=$HADOOP_HOME

export HADOOP_COMMON_LIB_NATIVE_DIR=$HADOOP_HOME/lib/native

export PATH=$PATH:$HADOOP_HOME/sbin:$HADOOP_HOME/bin

export HADOOP_INSTALL=$HADOOP_HOME

export HADOOP_OPTS="$HADOOP_OPTS -Djava.library.path=/home/student/hadoop/lib/"

export HADOOP_CLASSPATH=$JAVA_HOME/lib/tools.jar

###

source ~/.bashrc

sudo gedit etc/hadoop/hadoop-env.sh

###Update hadoop/etc/hadoop/hadoop-env.sh as follows

#### Add

export JAVA_HOME=/usr/lib/jvm/java-1.7.0-openjdk-i386

Replace:

export HADOOP_OPTS="$HADOOP_OPTS -Djava.net.preferIPv4Stack=true"

With:

export HADOOP_OPTS="$HADOOP_OPTS -XX:-PrintWarnings -Djava.net.preferIPv4Stack=true"

###Verify

###Update hadoop/etc/hadoop/core-site.xml

<configuration>

<property>

<name>fs.defaultFS</name>

<value>hdfs://localhost:9000</value>

</property>

</configuration>

cd /home/hadoop/etc/hadoop

mv mapred-site.xml.template mapred-site.xml

###Update hadoop/etc/hadoop/mapred-site.xml

<property>

<name>mapred.job.tracker</name>

<value>localhost:54311</value>

<description>The host and port that the MapReduce job tracker runs

at. If "local", then jobs are run in-process as a single map

and reduce task.

</description>

</property>

<property>

<name>mapreduce.framework.name</name>

<value>yarn</value>

</property>

####Update hadoop/etc/hadoop/yarn-site.xml

<configuration>

<property>

<name>yarn.nodemanager.aux-services</name>

<value>mapreduce_shuffle</value>

</property>

</configuration>

#### You should be in /home/student/hadoop

hadoop version

sudo chown student -R ./*

sbin/start-all.sh

#### sbin/start-dfs.sh - to start only dfs & yarn

#### sbin/start-yarn.sh

bin/hdfs namenode -format

jps #### jps should disply namenode, datanode, jobtracker etc.

Namenode - http://localhost:50070/ (in browser - dfs)

bin/hdfs dfs -mkdir /user

bin/hdfs dfs -mkdir /user/student

bin/hdfs dfs -mkdir /user/student/input

bin/hdfs dfs -put *.txt /user/student/input

bin/hdfs dfs -ls /user/student/input

bin/hadoop jar share/hadoop/mapreduce/hadoop-mapreduce-examples-2.7.2.jar wordcount input output

bin/hdfs dfs -ls output

bin/hdfs dfs -cat output/part-r-00000